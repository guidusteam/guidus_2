﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Intro : MonoBehaviour {

    Player_State player_state;
    Player_Animate player_animate;
    Caravan caravan;
    
    public GameObject intro_ui;
    public GameObject controller;

    public Text name, floor;
    bool start = false;

    void Start()
    {
        player_state = GameObject.Find("Player").GetComponent<Player_State>();
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();
        caravan = this.gameObject.GetComponent<Caravan>();

        player_animate.SendMessage("Freeze");

        name.text = "- " + player_state.name + "가문 " + player_state.generation.ToString() + "대 -";
        floor.text = "지하 "+ player_state.floor.ToString() + "층";

        intro_ui.SetActive(true);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !start)
        {
            //커지면서 사라짐
            start = true;
            intro_ui.transform.DOScale(2, 1.0f).OnComplete(() => GotoCaravan());
        }
    }

   void GotoCaravan()
    {
        //caravan 세팅
        caravan.enabled = true;
        caravan.SendMessage("Setting");
        
        //인트로 비활성화
        intro_ui.SetActive(false);
        this.enabled = false;

        //컨트롤러 활성화
        controller.SetActive(true);
    }
}
