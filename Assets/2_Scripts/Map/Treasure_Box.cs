﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Treasure_Box : MonoBehaviour {
    public Vector3 DEST = new Vector3(23,-0.4f,11);
    
    Item_Manager item_mng;
    public Draw_GUI gui;
    public ParticleSystem open_eff;
    public bool once = true;
    public bool is_enemy_die = false;
    public Set_Object set_obj;
    public Transform player_trans;

    int height = 20;

    public Animation action;
    public Shader shader;
    Material box_material;

    public Camera main_camera;
    Ray ray;
    RaycastHit hit;

    void Start()
    {
        action = this.gameObject.GetComponent<Animation>();
        box_material = new Material(shader);
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
    }
    

    public void DropBox()
    {
        Vector3 dest_pos = DEST;

        action.CrossFade("Close", 0.1f);

        if (player_trans.position.x == dest_pos.x && player_trans.position.z == dest_pos.z)
            dest_pos -= 4 * Vector3.left;

        //수직으로 낙하
        this.transform.position = dest_pos + Vector3.up * height;

        this.gameObject.transform.DOMoveY(dest_pos.y, 0.8f).SetEase(Ease.InCubic)
            .OnComplete(()=>this.gameObject.transform.DOPunchScale(new Vector3(0,-3,0),0.3f));
    }

    void OnMouseDown()
    {
        if ( once && is_enemy_die && Vector3.Distance(player_trans.position,transform.position) <= 3)
        {
            OpenBox();
        }
    }

    void OpenBox()
    {
        once = false;
        
        action.CrossFade("Open", 0.1f);

        item_mng.StartCoroutine("CreateItem");
        open_eff.Play();
    }

    public IEnumerator CloseBox()
    {
        //box_material.SetFloat("_Outline", 0);
        //제자리로 돌려놓고
        action.CrossFade("Close", 0.1f);
        this.gameObject.transform.position = DEST + Vector3.up * height;
        open_eff.Stop();

        yield return new WaitForSeconds(0.2f);

        once = true;
    }
}
