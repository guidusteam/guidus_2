﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Door : MonoBehaviour {
    public bool[] isDoor;
    public bool[] isOpened;
    public GameObject[] doors;
    public GameObject[] open_img;
    
    public Player_Animate player_animate;
    public Transform player_trans;
    public Maps maps;
    public bool enemy_die = false;

    public Camera main_camera;
    Ray ray;
    RaycastHit hit;



    public void EnemyDie()
    {
        enemy_die = true;

        for (int i = 0; i < doors.Length; ++i)
            if (isDoor[i])
            {
                isOpened[i] = true;
                OpenDoor(i);
            }
    }

/*
    void GetEvent()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = main_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("UI")))
                return;


            if (Physics.Raycast(ray, out hit, Mathf.Infinity,1<<LayerMask.NameToLayer("Door")))
            {
                //적이 모두 죽었을 때
                if (enemy_die)
                {
                    switch (hit.collider.tag)
                    {
                        case "Left":
                            player_animate.dest_pos = Define.door_pos[(int)Dir.left];
                            //player_animate.FindPath();
                         
                            break;

                        case "Right":
                            player_animate.dest_pos = Define.door_pos[(int)Dir.right];
                            //player_animate.FindPath();
                      
                            break;

                        case "Up":
                            player_animate.dest_pos = Define.door_pos[(int)Dir.up];
                            //player_animate.FindPath();
                          
                            break;

                        case "Down":
                            player_animate.dest_pos = Define.door_pos[(int)Dir.down];
                            //player_animate.FindPath();
                          
                           break;
                            
                    }
                }
            }
        }
   
    }
    */
    public void Reset()
    {
        for (int i = 0; i < doors.Length; ++i)
        {
            doors[i].SetActive(false);
            open_img[i].SetActive(false);

            isDoor[i] = false;
            isOpened[i] = false;

            //GridManager.instance.SendMessage("ResetFlrNode", Define.door_pos[i]);
            //GridManager.instance.SendMessage("ResetFlrNode", Define.door_pos[i + Define.door_count]);
        }
    }

    public void HideDoor()
    {
        for (int i = 0; i < doors.Length; ++i)
        {
            doors[i].SetActive(false);
            open_img[i].SetActive(false);
        }
    }

    public void OpenDoor(int num)
    {
        open_img[num].SetActive(true);
    }

    public void DrawDoor()
    {
        for (int i = 0; i < doors.Length; ++i)
        {
            if (isDoor[i])
            {
                
                if (isOpened[i]) OpenDoor(i);
                doors[i].SetActive(true);
                //GridManager.instance.CalculateFlr(Define.door_pos[i], FlrType.door);
                //GridManager.instance.CalculateFlr(Define.door_pos[Define.door_count + i], FlrType.door);
            }
        }
    }
}
