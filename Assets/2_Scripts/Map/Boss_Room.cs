﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Boss_Room : MonoBehaviour {

    public GameObject stair;
    public Door door;
    public GameObject[] boss;
    public GameObject[] npc;
    public Maps maps;

    Vector3[] set_pos;

    Player_Animate player_animate;
    Player_State player_state;
    Transform player_trans;
    Caravan caravan;
    GameObject prison;

    int boss_num;
    int npc_num;
    bool once = true;
    public Mini_Map minimaps;

    public Camera main_camera;

    Ray ray;
    RaycastHit hit;

    void Awake()
    {
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();
        player_state = GameObject.Find("Player").GetComponent<Player_State>();
        player_trans = GameObject.Find("Player").GetComponent<Transform>();
        caravan = GameObject.Find("Manager").GetComponent<Caravan>();
        boss_num = boss.Length;
        prison = GameObject.Find("Prison");
        prison.SetActive(false);

        set_pos = new Vector3[boss.Length];
        for (int i = 0; i < set_pos.Length; ++i)
        {
            set_pos[i] = boss[i].transform.position;
            print(set_pos[i]);
        }
    }

    void Update()
    {
        GetEvent();
    }
   
    void GetEvent()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = main_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {         
                if (hit.collider.tag == "Stair" && 
                    Vector3.Distance(stair.transform.position,player_trans.position) <= 10)
                {
                    stair.SetActive(false);

                    this.gameObject.SetActive(false);

                    player_animate.Setting(true);
                   // minimaps.SendMessage("Reset");
                    player_state.floor--;

                    caravan.enabled = true;
                    caravan.StartCoroutine("GotoCaravan");
                    maps.SendMessage("Reset");
                }
            }
        }           
    }

    public void CallBoss(){
        for (int i = 0; i < boss.Length; ++i)
        {
            boss[i].SetActive(true);
            boss[i].transform.position = set_pos[i];
            boss[i].SendMessage("SetTarget");
        }
        boss_num = boss.Length;
        once = true;

        //npc
        for (int i = 0; i < Define.NPC_FLOOR.Length; ++i) {
            //npc 구출해야하는 층이고 npc가 아직 활성화되지 않았을 때
            if (player_state.floor == Define.NPC_FLOOR[i] && caravan.npc[i].is_active == false)
            {
                npc_num = i;
                prison.SetActive(true);
                prison.transform.DOMoveY(0, 0);//제자리
                npc[npc_num].SetActive(true);
            }
       }
    }

    public bool CheckBoss(int died_num = 0)
    {
        boss_num += died_num;
        if(boss_num <= 0)
        {
            BossDied();
            return true;
        }
        return false;
    }

    public void KillBoss(bool player_die)
    {

        GridManager.instance.SendMessage("ResetObjNode", new Vector3(-1, -1, -1));

        for (int i = 0; i < boss.Length; ++i)
          boss[i].SetActive(false);

        if (!player_die) CheckBoss(-boss.Length);
    }

    public void BossDied()
    {
        if (once)
        {
            stair.SetActive(true);
            door.SendMessage("EnemyDie");
            once = false;

            prison.transform.DOMoveY(-4, 0.5f);
            //구출
            caravan.npc[npc_num].is_active = true;
        }
    }
}
