﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Set_Object : MonoBehaviour {
    
    public GameObject[] enemy;
    Player_Animate player_animate;
    Transform player;
    public Door door;
    Vector3[] set_pos;  //초기화를 위해 초기 위치 저장

    int enemy_num;

    bool once = true;
    GameObject treasure_box;
    Treasure_Box treasure_scrt;
    public Corpse corpse;    

    void Awake()
    {
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();
        player = GameObject.Find("Player").GetComponent<Transform>();

        //적 초기 위치 저장
        set_pos = new Vector3[enemy.Length];
        enemy_num = enemy.Length;

        for (int i = 0; i < enemy.Length; ++i)
        {
            set_pos[i] = enemy[i].transform.position;
        }
        //현재 보물방일 경우
        if (this.gameObject.CompareTag("Treasure_Room"))
        {
            treasure_box = GameObject.FindGameObjectWithTag("Treasure_Box");
        }
    }
    
    public void CallEnemy()
    {
        for (int i = 0; i < enemy.Length; ++i)
        {
            enemy[i].SetActive(true);
            enemy[i].transform.position = set_pos[i];
            enemy[i].SendMessage("SetTarget");
        }

        enemy_num = enemy.Length;
        //현재 보물방일 경우
        if (this.gameObject.CompareTag("Treasure_Room"))
        {
            treasure_box.SetActive(true);
        }
        corpse.Active();
        once = true;
    }

    public bool CheckEnemy(int died_num = 0)
    {
        enemy_num += died_num;
        if (enemy_num <= 0)
        {
            EnemyDied();
            return true;
        }
        return false;
    }
    
    public void EnemyDied()
    {
        if (once)
        {
            //다 죽었다면 위치 처음으로 돌려줌
            for (int i = 0; i < enemy.Length; ++i)
            {
                enemy[i].transform.position = set_pos[i];
            }
            door.EnemyDie();
            corpse.RemoveBarricades();
            once = false;

            if (this.gameObject.CompareTag("Treasure_Room"))
            {
                treasure_scrt = treasure_box.GetComponent<Treasure_Box>();
                treasure_scrt.is_enemy_die = true;
                treasure_scrt.DropBox();
            }

        }
    }

    public void KillEnemy( bool player_die)
    {
        GridManager.instance.SendMessage("ResetObjNode", new Vector3(-1, -1, -1));
        for (int i = 0; i < enemy.Length; ++i)
        {
            enemy[i].SetActive(false);
        }
        if(!player_die) CheckEnemy(-enemy.Length);
    }

}
