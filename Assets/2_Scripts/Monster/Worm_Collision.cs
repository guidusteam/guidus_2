﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Worm_Collision : MonoBehaviour {

    public float health = 30;
    Player_State Player_State;
    Worm_Animate worm_animate;
    float damage;

    void Start()
    {
        worm_animate = GetComponent<Worm_Animate>();
        Player_State = GameObject.FindGameObjectWithTag("Player_Character").GetComponent<Player_State>();
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.tag == "Bullet")
        {
            damage = Player_State.attack_power;
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = 30;
        this.transform.DOKill();
        worm_animate.StartCoroutine("Die");
    }

}
