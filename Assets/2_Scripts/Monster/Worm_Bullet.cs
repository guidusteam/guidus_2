﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Worm_Bullet : MonoBehaviour {
    public Transform shoot_point;
    public MeshRenderer draw;
    public Collider collider;


    void Start()
    {
        draw = this.gameObject.GetComponent<MeshRenderer>();
        collider = this.gameObject.GetComponent<Collider>();
        draw.enabled = false;
        collider.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        //적이나 장애물, 플레이어에 충돌하면 제자리로, 
        //총알끼리, 문에 충돌하면 무시!
        if (other.CompareTag("Obstacle") || other.CompareTag("Player_Character") || other.CompareTag("Collider_Wall"))
            Reset();
    }


    public void Reset()
    {
        draw.enabled = false;
        collider.enabled = false;
        this.transform.DOKill();
        this.transform.position = shoot_point.position;
    }
}
