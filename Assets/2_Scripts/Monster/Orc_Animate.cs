﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Orc_Animate : MonoBehaviour
{
    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
    Set_Object set_obj;
    Item_Manager item_mng;
    GameObject player;
    Player_State player_state;
    Animator orc_action;

    float move_speed = 0.5f;
    bool attack = false;

    int attack_power = 8;

    // Use this for initialization

    void SetTarget()
    {
        player = GameObject.FindGameObjectWithTag("Player_Character");
        player_state = player.GetComponent<Player_State>();
        orc_action = this.gameObject.GetComponent<Animator>();
        set_obj = GetComponentInParent<Set_Object>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();

    }


    void FindPath()
    {
        startPos = this.transform;
        endPos = player.transform;
        endPos.position = new Vector3(player.transform.position.x, 0, player.transform.position.z);

        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);

        if (pathArray.Count <= 2) StartCoroutine("Attack");
        else Move();


    }

    void Move()
    {
        attack = false;
        orc_action.CrossFade("Run", 0.2f);
        orc_action.SetBool("attack", false);
        orc_action.SetBool("run", true);
        Node node = (Node)pathArray[1];

        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", transform.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        this.transform.DOMove(node.position, move_speed).SetEase(Ease.Linear).OnComplete(() => FindPath());// StartCoroutine("Break"));

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(node.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        this.transform.DORotate(dest_rot, move_speed);
    }

    IEnumerator Attack()
    {
        attack = true;
        orc_action.SetBool("run", false);
        orc_action.SetBool("attack", true);
        Quaternion look_rot = Quaternion.LookRotation(player.transform.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        this.transform.DORotate(dest_rot, 0.2f);
        yield return new WaitForSeconds(0.5f);

        //거리 안에 있으면
        if ((player.transform.position - this.transform.position).magnitude <= Define.ATTACK_BOUND)
        {
            player_state.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.5f);

        FindPath();
    }

    IEnumerator Die()
    {
        orc_action.CrossFade("Die", 0.3f);
        yield return new WaitForSeconds(0.6f);

        GridManager.instance.SendMessage("ResetObjNode", transform.position);
        set_obj.CheckEnemy(-1);
        int rand = Random.Range(0, 10000);
        if (rand < 1000)
            item_mng.DropItem(transform.position);
        this.gameObject.SetActive(false);
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            foreach (Node node in pathArray)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(node.position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}