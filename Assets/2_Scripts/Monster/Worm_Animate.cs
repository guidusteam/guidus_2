﻿using UnityEngine;
using System.Collections;

public class Worm_Animate : MonoBehaviour {

    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
    //int[] obs_index;
    public GameObject shadow;

    Animation worm_Anim;
    BoxCollider worm_collider;

    Set_Object set_obj;
    Item_Manager item_mng;

    public string my_type;
    public Worm_Shoot shoot;

    int turn = 0; //up한 후 2번 공격하고 들어갈 수 있게

    void SetTarget() {
//        print("SET!!!");
        worm_Anim = GetComponent<Animation>();
        worm_collider = this.gameObject.GetComponent<BoxCollider>();
        set_obj = GetComponentInParent<Set_Object>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
        StartCoroutine("Up");
    }

    IEnumerator Up()
    {
//        print("up시작");
        worm_collider.enabled = true;

        worm_Anim.CrossFade("Worm_Up");
        shadow.SetActive(true);

        yield return new WaitForSeconds(0.8f);

        
        StartCoroutine("Idle");
//        print("up종료");
    }

    IEnumerator Idle()
    {
//        print("idle 시작"+Time.time);
        worm_Anim.CrossFade("Worm_Idle");

        yield return new WaitForSeconds(1.0f);

        
        StartCoroutine("Attack");
//        print("idle 종료" + Time.time);

    }

    IEnumerator Attack()
    {
//        print("attack 시작" + Time.time);
        worm_Anim.CrossFade("Worm_Attack");

        //탄환 발사
        if (my_type == "2way") shoot.StartCoroutine("ShootBullet_2way");
        else if (my_type == "4way") shoot.StartCoroutine("ShootBullet_4way");
        else if (my_type == "chasing") shoot.StartCoroutine("ShootBullet_chasing");

        yield return new WaitForSeconds(1.0f);

        

        //StartCoroutine("Hide");
        ++turn;
        Check();
//        print("attack 종료" + Time.time);
    }

    void Check()
    {
        if (turn < 2)
        {
            StartCoroutine("Idle");
            return;
        }
        else
        {
            turn = 0;
            StartCoroutine("Hide");
            return;
        }
    }

    IEnumerator Hide()
    {
        
        bool pass = false;
//        print("hide 시작" + Time.time);
        worm_Anim.CrossFade("Worm_Hide");
        worm_collider.enabled = false;
        shadow.SetActive(false);

        GridManager.instance.SendMessage("ResetObjNode", transform.position);

        yield return new WaitForSeconds(1.0f);

        while (!pass)   //장애물, 적, 플레이어와 겹치는 위치에서 올라오지 않게 함
        {
            pass = true;

            //11~33범위, 7~17범위에서 홀수만!
            this.transform.position = new Vector3(Random.Range(5, 16) * 2 + 1, 0, Random.Range(3, 8) * 2 + 1);

            Vector2 rowcol = GridManager.instance.GetNodeRowCol(transform.position);
            if (GridManager.instance.nodes[(int)rowcol.x, (int)rowcol.y].objtype != ObjType.empty)
                pass = false;
        }
        yield return new WaitForSeconds(4.0f);

        GridManager.instance.CalculateObj(transform.position, ObjType.enemy);
        StartCoroutine("Up");
//        print("hide 종료!!!" + Time.time);
    }

    IEnumerator Die()
    {
        worm_Anim.CrossFade("Worm_Hide");
        yield return new WaitForSeconds(0.6f);

        GridManager.instance.SendMessage("ResetObjNode", transform.position);
        set_obj.CheckEnemy(-1);

        int rand = Random.Range(0, 10000);
        if(rand < 1000)
            item_mng.DropItem(transform.position);
        this.gameObject.SetActive(false);
    }
}
