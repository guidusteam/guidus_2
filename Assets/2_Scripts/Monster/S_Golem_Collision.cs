﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class S_Golem_Collision : MonoBehaviour
{
    public float health = 30;
    Player_State player_state;
    S_Golem_Animate s_golem_animate;
    float damage;

    void Start()
    {
        health = 30;
        s_golem_animate = GetComponent<S_Golem_Animate>();
        player_state = GameObject.FindGameObjectWithTag("Player_Character").GetComponent<Player_State>();
        damage = player_state.attack_power;
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.tag == "Bullet")
        {
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = 30;
        this.transform.DOKill();
        s_golem_animate.StartCoroutine("Die");
    }
}
