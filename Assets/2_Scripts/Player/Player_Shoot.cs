﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Player_Shoot : MonoBehaviour
{

    public bool active = false;
    public Animator player_anim;
    Player_State player_state;
    public GameObject[] Bullet;
    Collider[] bullet_collider;
    Transform[] bullet_trans;
    MeshRenderer[] bullet_draw;
    Bullet[] bullet_scrt;
    TrailRenderer[] trail;
    public Transform player;

    
    //==============================
    int bullet_num = 10;
    public int bullet_dist = 50;
     int curve_dist = 4;      //파장  작을수록
     int curve = 2;           //진폭  클수록 크게 진동
    public int shoot_num = 1;
    //==============================
    float shoot_time = 0;
    Vector3 shoot_dir;
    
    public Camera main_camera;
    Ray ray;
    RaycastHit hit;
    void Start()
    {
        player_state = player.gameObject.GetComponent<Player_State>();
        bullet_collider = new Collider[bullet_num];
        bullet_trans = new Transform[bullet_num];
        bullet_draw = new MeshRenderer[bullet_num];
        bullet_scrt = new Bullet[bullet_num];
        trail = new TrailRenderer[bullet_num];

        for (int i = 0; i < bullet_num; ++i)
        {
            bullet_collider[i] = Bullet[i].GetComponent<Collider>();
            bullet_scrt[i] = Bullet[i].GetComponent<Bullet>();
            bullet_trans[i] = Bullet[i].GetComponent<Transform>();
            bullet_draw[i] = Bullet[i].GetComponent<MeshRenderer>();
            trail[i] = Bullet[i].GetComponent<TrailRenderer>();
        }
    }

    public void ShootBullet(Vector3 dir, bool move) { 
        if (Time.time - shoot_time > 0.7f)
        {
            if (!move)
            {
                player_anim.SetBool("shoot", true);
                player_anim.CrossFade("attack", 0.1f);
            }
            shoot_dir = dir;
            shoot_time = Time.time;
            for (int i = 0; i < bullet_num; ++i)
            {
                if (!bullet_scrt[i].in_use)
                {
                    bullet_scrt[i].in_use = true;
                     if (player_state.shoot_path == ShootPath.linear)
                         LinearShoot(i);
                     else if (player_state.shoot_path == ShootPath.curve)
                         CurveShoot(i);
                     else if(player_state.shoot_path == ShootPath.spiral)
                        SpiralShoot(i);
                    return;
                }
            }
        }
    }

    void LinearShoot(int num)
    {
        bullet_draw[num].enabled = true;
        bullet_collider[num].enabled = true;
        trail[num].enabled = true;
        bullet_trans[num].position = transform.position;
        bullet_trans[num].DOMove(transform.position + shoot_dir * bullet_dist, player_state.shoot_speed).SetEase(Ease.Linear).OnComplete(() => Reset(num));
        player_anim.SetBool("shoot", false);
    }

    void CurveShoot(int num)
    {
        //경로 찍기
        bullet_trans[num].transform.position = transform.position;
        Vector3 dir = Vector3.Cross(shoot_dir, Vector3.up).normalized;

        Vector3[] path = new Vector3[bullet_dist / curve_dist];

        for (int i = 0; i < bullet_dist / curve_dist; ++i)
        {
            if (i % 2 == 0) path[i] = bullet_trans[num].position + dir * curve + shoot_dir * curve_dist * i;
            else path[i] = bullet_trans[num].position - dir * curve + shoot_dir * curve_dist * i;
        }
        
        bullet_draw[num].enabled = true;
        bullet_collider[num].enabled = true;
        trail[num].enabled = true;
        bullet_trans[num].DOPath(path, player_state.shoot_speed,PathType.CatmullRom).SetEase(Ease.Linear).OnComplete(()=>Reset(num));
        player_anim.SetBool("shoot", false);
    }

    void SpiralShoot(int num)
    {
        bullet_draw[num].enabled = true;
        bullet_collider[num].enabled = true;
        trail[num].enabled = true;
        bullet_trans[num].position = transform.position;
        bullet_trans[num].DOSpiral(player_state.shoot_speed*2, Vector3.up, SpiralMode.Expand,1,2).OnComplete(() => Reset(num));
        player_anim.SetBool("shoot", false);
    }
    
    void Reset(int b_num)
    {
        bullet_scrt[b_num].StartCoroutine("Reset");
    }
}