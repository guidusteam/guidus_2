﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Player_State : MonoBehaviour {
    public string name;
    public int floor = 99, generation = 1;
    public int gold = 1000;

    public float health, stamina;
    public float max_health = Define.MAX_HEALTH, max_stamina = Define.MAX_STAMINA;
    public float move_speed = Define.MOVE_SPEED, shoot_speed = Define.SHOOT_SPEED;
    public float attack_power = Define.ATTACK_POWER;
    public ShootPath shoot_path = ShootPath.linear;


    public class Changed
    {
        public float max_health = 0, max_stamina = 0;
        public float move_speed = 0, shoot_speed = 0;
        public float attack_power = 0;
    }

    public Changed changed;
    public Boss_Room boss_room;

    public Text die_msg;
    public TextMesh head_3d;
    public Animator player_anim;
    public ParticleSystem die_eff;
    GameObject manager;
    Player_Revive player_revive;
    Player_Animate player_animate;
    Controller controller;
    GameObject main_camera;
    Maps maps;
    Show_State show_state;
    Fade fade;

    float stamina_time = 0;

    void Start()
    {
        manager = GameObject.Find("Manager");
        fade = manager.GetComponent<Fade>();
        show_state = manager.GetComponent<Show_State>();
        player_revive = this.gameObject.GetComponent<Player_Revive>();
        player_animate = this.gameObject.GetComponent<Player_Animate>();
        maps = GameObject.Find("Maps").GetComponent<Maps>();
        controller = manager.GetComponent<Controller>();
        main_camera = GameObject.Find("Main Camera");

        changed = new Changed();
        health = max_health;
        stamina = max_stamina;
    }

    void Update()
    {
        head_3d.transform.rotation = Quaternion.Euler(40, 0, 0);
        //0.1초 
        if (Time.time - stamina_time > 0.1f && stamina < max_stamina + changed.max_stamina)
        {
            stamina += 0.1f;
            stamina_time = Time.time;

            show_state.Update_ST();
        }
    }

    public void Update_hp(float num = 0)
    {
        health += num;
        health = Mathf.Clamp(health, 0, max_health+changed.max_health);
        show_state.Update_HP();

        if (num != 0)
        {
            head_3d.text = "HP  " + num.ToString();
            //fade.StartCoroutine("FadeIn", head_3d.color);
            show_state.StartCoroutine("ShowHeadText", head_3d);
        }

        //죽음
        if (health <= 0)
            StartCoroutine("Die");
    }

    public void Update_gold(int num)
    {
        gold += num;
              
        head_3d.text = "GOLD  " + num.ToString();
        show_state.StartCoroutine("ShowHeadText", head_3d);
        
    }

    public void Update_st(float num = 0)
    {
        stamina += num;

        stamina = Mathf.Clamp(stamina, 0, max_stamina+changed.max_stamina);
        show_state.Update_ST();

        if (num != 0)
        {
            head_3d.text = "STAMINA  " + num.ToString();
            show_state.StartCoroutine("ShowHeadText", head_3d);
        }
    }

    public void Update_ui()
    {
        show_state.SendMessage("UpdateAbility");
        show_state.SendMessage("UpdateFloor");
    }

    public void ChangedValueReset()
    {
        changed.max_health = 0;
        changed.max_stamina = 0;
        changed.move_speed = 0;
        changed.shoot_speed = 0;
        changed.attack_power = 0;
    }

    public void SetValue(float max_hp, float max_st, float att, float m_spd, float s_spd)
    {
        max_health += max_hp;
        max_stamina += max_st;
        attack_power += att;
        move_speed += m_spd;
        shoot_speed += s_spd;
    }

    IEnumerator Die()
    {
        controller.pause = true;
        //~~대가 죽었다.
        die_msg.gameObject.SetActive(true);
        die_msg.text = name + "가문 " + generation.ToString() + "대가 죽었다.";

        //플레이어 Die 애니메이션
        player_anim.CrossFade("die", 0.3f);

        yield return new WaitForSeconds(0.5f);

        die_eff.Play();

        yield return new WaitForSeconds(1.0f);

        

        //적 없애기
        if (maps.room_num == Define.ROOM_NUM - 1)   //보스방
            boss_room.KillBoss(true);
        else
            maps.set_obj[maps.room_num].KillEnemy(true);

        //플레이어 멈추기
        player_animate.SendMessage("Freeze");

        yield return new WaitForSeconds(1.0f);

        die_msg.gameObject.SetActive(false);


        //팝업
        player_revive.Setting();
        player_revive.StartCoroutine("ShowWindow", true);
    }
}