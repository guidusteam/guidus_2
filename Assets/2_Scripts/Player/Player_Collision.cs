﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Player_Collision : MonoBehaviour {

    Player_State player_state;
    Player_Animate player_animate;
    Item_Manager item_mng;
    Caravan caravan;
    Maps maps;

    void Start()
    {
        player_state = GetComponent<Player_State>();
        player_animate = GetComponent<Player_Animate>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
        caravan = GameObject.Find("Manager").GetComponent<Caravan>();
        maps = GameObject.Find("Maps").GetComponent<Maps>();
    }

    void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "Worm_Bullet":
                player_state.Update_hp(-3);
                break;

            case "Gold":
                StartCoroutine("PickUp", other);
                break;

            case "HP":
                StartCoroutine("PickUp", other);
                break;

            case "Left":
                if (maps.door_scrt[maps.room_num].isOpened[(int)Dir.left])
                    maps.MoveMap((int)Dir.left);
                break;

            case "Right":
                if (maps.door_scrt[maps.room_num].isOpened[(int)Dir.right])
                    maps.MoveMap((int)Dir.right);
                break;

            case "Up":
                if (maps.door_scrt[maps.room_num].isOpened[(int)Dir.up])
                    maps.MoveMap((int)Dir.up);
                break;

            case "Down":
                if (maps.door_scrt[maps.room_num].isOpened[(int)Dir.down])
                    maps.MoveMap((int)Dir.down);
                break;

            case "Exit":
                caravan.StartCoroutine("StartGame");
                break;

            case "Obstacle":
                transform.DOFlip();
                break;

            case "NPC":
                break;
        }
    }

    IEnumerator PickUp(Collider obj)
    {
        player_animate.player_anim.CrossFade("pickup",0.3f);

        yield return new WaitForSeconds(0.5f);

        if (obj.CompareTag("Gold"))
            player_state.Update_gold(10);
        else if (obj.CompareTag("HP"))
            player_state.Update_hp(10);

        item_mng.HideItem(obj.transform.position);
        player_animate.Reset();
    }
}
