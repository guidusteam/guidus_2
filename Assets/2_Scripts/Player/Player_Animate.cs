﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Player_Animate : MonoBehaviour {

    public Camera main_camera;

    public Transform camera_trans;
    public Animator player_anim;

    public ParticleSystem start_eff;
    public Maps maps;

    Caravan caravan;
    int[] obs_index;

    Player_State player_state;
    BoxCollider player_collider;
    NPC_Work npc_work;

    Vector3 dest_rot;
    public Vector3 dir;
    public Vector3 dest_pos;

    public bool is_active;
    public bool in_caravan = true;
    Quaternion look_rot;
    bool double_click = false;  //더블클릭 판별


    int rolling_length = 7;
    float rolling_speed = 0.5f;
    int click_num = 0;
    float[] time = { -1, -1 };
    Ray ray;
    RaycastHit hit;
    void Start()
    {
        obs_index = new int[GridManager.instance.obstacleList.Length];
        player_state = GetComponent<Player_State>();
        player_collider = GetComponent<BoxCollider>();
        caravan = GameObject.Find("Manager").GetComponent<Caravan>();
        npc_work = GameObject.Find("Manager").GetComponent<NPC_Work>();
    }

    void UnFreeze() {

        player_anim.enabled = true;
        player_anim.CrossFade("idle", 0.3f);
        Setting(true);
    }

    void Freeze()
    {
        player_anim.enabled = false;
        is_active = false;
    }

    public void Setting(bool first = false)
    {
        transform.DOKill();
        transform.position = new Vector3(Define.HCENTER, 0, Define.VCENTER);
        transform.eulerAngles = new Vector3(0, 90, 0);

        camera_trans.DOKill();
        if (in_caravan) camera_trans.position = new Vector3(23, 50, -46);
        else camera_trans.position = new Vector3(Define.HCENTER, Define.C_POS_Y, Define.C_POS_Z);

        is_active = true;

        player_anim.SetBool("run", false);
        player_anim.SetBool("rolling", false);

        //dest_pos = transform.position;

        obs_index = new int[GridManager.instance.obstacleList.Length];
        for (int i = 0; i < GridManager.instance.obstacleList.Length; ++i)
            obs_index[i] = GridManager.instance.GetGridIndex(GridManager.instance.obstacleList[i].transform.position);

        if (first)
        {
            start_eff.transform.position = transform.position;
            start_eff.Play();
        }

    }

    public void Reset(bool move = false, int dir = 0)
    {
        transform.DOKill();
        camera_trans.DOKill();
        player_anim.CrossFade("idle", 0.3f);
        player_anim.SetBool("run", false);
        player_anim.SetBool("rolling", false);
        player_collider.enabled = true;

        if (move)   //맵 이동 전 리셋인 경우
            maps.MoveMap(dir);
    }

    void Update()
    {
        GetEvent();
    }

    void GetEvent() {
        if (Input.GetMouseButtonDown(0)) //터치
        {
            ray = main_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("UI")) ||
                Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Door")))
            {
                //UI터치는 무시, 문 터치는 Door스크립트에서 처리
                return;
            }
            else if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Floor")))
            {
                if (hit.collider.CompareTag("Obstacle")) return;

                //번갈아가면서 저장할 수 있게
                click_num = (click_num + 1) % 2;

                //시간비교해서 더블클릭 판별
                time[click_num % 2] = Time.time;
                float delta_time = Mathf.Abs(time[0] - time[1]);
                if (delta_time < 0.2f)
                    double_click = true;


                dest_pos = new Vector3(hit.point.x, this.transform.position.y, hit.point.z);

                //최대 최소에 맞춰 범위 수정
                dest_pos.x = Mathf.Clamp(dest_pos.x, 1, Define.HMAX - 1);
                dest_pos.z = Mathf.Clamp(dest_pos.z, 1, Define.VMAX - 1);

                //더블클릭일때 스태미너값 비교해서 실행판단
                if (double_click && player_state.stamina >= 10)
                {
                    Rolling();
                    return;
                }
                else if (double_click && player_state.stamina < 10)
                {
                    double_click = false;
                    return;
                }

                //FindPath();

            }
        }
    }

    public void Move(Vector3 move_dir)
    {
        DOTween.Kill("trans");
        //장애물, npc, 보물상자, 시체, 구멍, 맵 경계
        if (Physics.Raycast(transform.position, move_dir, out hit, 2) &&
            (hit.collider.CompareTag("Obstacle") || hit.collider.CompareTag("NPC") || hit.collider.CompareTag("Treasure_Box")
            || hit.collider.CompareTag("Corpse") || hit.collider.CompareTag("Hole") || hit.collider.CompareTag("Collider_Wall")
            || hit.collider.CompareTag("Enemy")))
            return;
        
        //움직임
        else {
            dest_pos = transform.position + move_dir * Define.MOVE_DIST;
            transform.DOMove(dest_pos, player_state.move_speed).SetEase(Ease.Linear).SetId("trans");

            //카라반에선 카메라움직임X
            if (in_caravan) return;

            //카메라
            camera_trans.DOKill();

            if (transform.position.x >= Define.C_HMIN && transform.position.x <= Define.C_HMAX)
                camera_trans.DOMoveX(dest_pos.x, player_state.move_speed).SetEase(Ease.Linear);

            if (transform.position.z >= Define.C_VMIN && transform.position.z <= Define.C_VMAX)
                camera_trans.DOMoveZ(dest_pos.z - Define.VCENTER + Define.C_POS_Z, player_state.move_speed).SetEase(Ease.Linear);
        }
    }

    public void Rotate(Vector3 angle, bool shooting)
    {
        DOTween.Kill("rot");
        if(shooting)
            transform.DORotate(angle, 0.02f).SetId("rot");
        else
            transform.DORotate(angle, player_state.move_speed).SetId("rot");
    }
    
    void Rolling()
    {
        //caravan에선 구르기X
        if (in_caravan) return;

        //가는 길에 장애물이 있으면 X
        if (Physics.Raycast(transform.position, dest_pos, out hit, rolling_length) &&
            hit.collider.CompareTag("Obstacle")) return;

        //마우스 방향으로 회전
            look_rot = Quaternion.LookRotation(dest_pos - this.transform.position);
        dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;
        this.transform.DORotate(dest_rot, rolling_speed);

        //현재위치->목표위치 방향벡터 정규화
        dest_pos -= this.transform.position;
        dest_pos.Normalize();

        //해당 방향으로 7만큼
        dest_pos = this.transform.position + dest_pos * rolling_length;

        if (1 > dest_pos.x || dest_pos.x > Define.HMAX - 1 ||
                         1 > dest_pos.z || dest_pos.z > Define.VMAX - 1)
        {
            dest_pos.x = Mathf.Clamp(dest_pos.x, 1, Define.HMAX - 1);
            dest_pos.z = Mathf.Clamp(dest_pos.z, 1, Define.VMAX - 1);

        }      
        //목표지점이 빈곳이 아니면 실행X
        if (GridManager.instance.GetObjType(dest_pos) != ObjType.empty) return;

        player_anim.SetBool("rolling", true);
        player_collider.enabled = false;

        this.transform.DOKill();
        transform.DOMove(dest_pos, rolling_speed).SetEase(Ease.OutCubic).OnComplete(() => Reset());

        double_click = false;
        player_state.Update_st(-10);

        //카메라이동
        camera_trans.DOKill();
        dest_pos.x = Mathf.Clamp(dest_pos.x, Define.C_HMIN, Define.C_HMAX);
        camera_trans.DOMoveX(dest_pos.x, rolling_speed);

        if (dest_pos.z <= Define.C_VMIN)
            camera_trans.DOMoveZ(Define.C_VMIN-Define.VCENTER + Define.C_POS_Z, rolling_speed);
        else if (dest_pos.z >= Define.C_VMAX)
            camera_trans.DOMoveZ(Define.C_VMAX - Define.VCENTER+ Define.C_POS_Z, rolling_speed);
    }
}
