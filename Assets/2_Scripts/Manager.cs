﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class Manager : MonoBehaviour {
    private Manager _ins;

    public Manager Instance
    {
        get
        {
            if(_ins == null)
                _ins = GameObject.FindObjectOfType(typeof(Manager)) as Manager;
            return _ins;
        }
    }
    void Start()
    {
        //ads = Player.GetComponent<AudioSource>();
        //background_sound = GetComponent<AudioSource>();

        if (Advertisement.isSupported)
        {
            Advertisement.allowPrecache = true;
#if UNITY_ANDROID
            Advertisement.Initialize("114276", false);	//(appID, TestMode)
#elif UNITY_IOS
			;
#endif
        }
        else
        {
            Debug.Log("Platform not supported");
        }
  
    }
}
