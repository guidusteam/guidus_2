﻿using UnityEngine;
using System.Collections;
//using GooglePlayGames;
//using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class GooglePlayGameManager
{
    private static GooglePlayGameManager Getinstance = null;
    public static GooglePlayGameManager Instance
    {
        get
        {
            if (Getinstance == null)
                Getinstance = new GooglePlayGameManager();
            return Getinstance;
        }
    }
    /*
    string log;

    // 로그인 판별 드래그 값
    public bool bLogin
    {
        get;
        set;
    }

    // 첫 시작 GPG 초기화
    public void InitializeGPGS()
    {
        bLogin = false;

        PlayGamesPlatform.Activate();
    }

    // GPGS 로그인
    public void LoginGPGS()
    {
        // 로그인이 안되어 있다면
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate(LoginCallBackGPGS);
        }
    }

    // 로그 아웃
    public void LogOutGPGS()
    {
        // 로그인이 되어 있다면
        if (Social.localUser.authenticated)
        {
            ((GooglePlayGames.PlayGamesPlatform)Social.Active).SignOut();
            bLogin = false;
        }
    }

    // GPGS 에서 자신의 프로필 사진을 가져 옵니다.
    public Texture2D GetImageGPGS()
    {
        // 로그인이 되어있다면 이미지를 리턴하고 아니면 널을 리턴
        if (Social.localUser.authenticated)
            return Social.localUser.image;
        else
            return null;
    }

    // GPGS 에서 사용자의 이름을 가져옵니다.
    public string GetNameGPGS()
    {
        if (Social.localUser.authenticated)
            return Social.localUser.userName;
        else
            return null;
    }

    // 자신의 업적&리더보드 UI를 불러 옵니다.
    public void ShowAchievmentUI() { Social.ShowAchievementsUI(); }  ///업적 보기
	public void ShowLeaderBoard() { Social.ShowLeaderboardUI(); }   /// 리더보드 띄우기

    // 로그인 드래그 콜백함수
    public void LoginCallBackGPGS(bool result) { bLogin = result; }

    //post score 12345 to leaderboard ID "Cfji293fjsie_QA")

    public void LeaderBoard(int score)
    {
        Social.ReportScore(score, "CgkIoLmrxuQFEAIQAQ", (bool success) =>
        {

            //handle success or failure

        });
    }
    */
}
