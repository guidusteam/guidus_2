﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Bullet : MonoBehaviour {

    public Transform shoot_point;
    public bool in_use;
    Player_State player_state;
    MeshRenderer draw;
    Collider collider;
    TrailRenderer trail;
    public ParticleSystem hit_eff;

    void Start()
    {
        draw = this.gameObject.GetComponent<MeshRenderer>();
        collider = this.gameObject.GetComponent<Collider>();
        draw.enabled = false;
        collider.enabled = false;
        player_state = GameObject.Find("Player").GetComponent<Player_State>();
        trail = GetComponent<TrailRenderer>();
        in_use = false;
    }

    void OnTriggerEnter(Collider other)
    {
        //적이나 장애물에 충돌하면 제자리로, 
        //총알끼리, 문에 충돌하면 무시!
        if (other.CompareTag("Obstacle") || other.CompareTag("Collider_Wall") || other.CompareTag("Floor"))
            StartCoroutine("Reset");
        else if(other.CompareTag("Enemy"))
        {
            hit_eff.transform.position = transform.position;
            hit_eff.Play();
            StartCoroutine("Reset");
        }
    }


    IEnumerator Reset()
    {
        draw.enabled = false;
        collider.enabled = false;
        trail.enabled = false;
        this.transform.DOKill();

        yield return new WaitForSeconds(0.5f);
        
        this.transform.position = shoot_point.position;
        in_use = false;
    }
}
