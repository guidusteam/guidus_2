﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class Caravan : MonoBehaviour {

    public GameObject game_ui;
    public GameObject caravan_ui;
    public GameObject caravan_map;
    public GameObject game_door;
    public GameObject exit_door;
    public Text floor;
    public Text Gold;
    public Maps maps;
    Player_State player_state;
    Player_Animate player_animate;
    //Fade fade;
    CanvasGroup floor_cvs;
    Controller controller;
    Camera main_camera;
    Ray ray;
    RaycastHit hit;

    GameObject[] npcs;  //tag검사해서 npc object넣기
    public NPC[] npc;

    void Start () {
        caravan_ui.transform.DOScale(2.0f, 0.0f);
        game_ui.transform.DOScale(2.0f, 0.0f);
        floor_cvs = floor.GetComponent<CanvasGroup>();
        controller = GetComponent<Controller>();

        main_camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        main_camera.transform.position = new Vector3(23, 50, -46);

        npcs = GameObject.FindGameObjectsWithTag("NPC");
        npc = new NPC[npcs.Length];
        for (int i = 0; i < npcs.Length; ++i)   //스크립트만 받아오고 비활
        {
            npc[i] = npcs[i].GetComponent<NPC>();
            npcs[i].SetActive(false);
        }


        player_state = GameObject.Find("Player").GetComponent<Player_State>();
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();
        player_animate.in_caravan = true;
        //fade = this.gameObject.GetComponent<Fade>();
        this.enabled = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = main_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("UI")))
                return;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Door")))
            {
               // player_animate.dest_pos = Define.door_pos[(int)Dir.down];
                //player_animate.FindPath();
            }
        }
    }

    void Setting()
    {
        //caravan UI 활성화
        caravan_ui.SetActive(true);
        caravan_ui.transform.DOScale(1.0f, 0.5f);

        //구출한 npc 표시
        foreach(NPC n in npc)
        {
            if (n.is_active)
            {
                n.gameObject.SetActive(true);
                n.StartCoroutine("ShowMent");
                GridManager.instance.CalculateObj(n.transform.position, ObjType.NPC);
            }
        }

        //층
        floor.text = player_state.floor + "층";
        floor_cvs.alpha = 0;

        //코인
        Gold.text = "Gold : " + player_state.gold;
        
        //출구 활성화
        exit_door.SetActive(true);
        //GridManager.instance.CalculateFlr(Define.door_pos[(int)Dir.down], FlrType.exit);
        //GridManager.instance.CalculateFlr(Define.door_pos[Define.door_count + (int)Dir.down], FlrType.exit);


        //플레이어 애니메이션, 이동 가능하게
        player_animate.SendMessage("UnFreeze");
    }

    public IEnumerator GotoCaravan()
    {
        print("Caravan");
        // 플레이어 애니메이션, 이동 가능하게
        player_animate.in_caravan = true;
        player_animate.SendMessage("UnFreeze");

        Fade.instance.FadeOut();

        yield return new WaitForSeconds(0.5f);

        //게임 ui, door 비활성
        game_ui.SetActive(false);
        game_door.SetActive(false);

        //층
        floor.text = player_state.floor + "층";
        floor_cvs.alpha = 0;

        //ui, 맵 활성
        caravan_ui.SetActive(true);
        caravan_map.SetActive(true);

        //층
        floor.text = player_state.floor + "층";
        floor_cvs.alpha = 0;

        //코인
        Gold.text = "Coin : " + player_state.gold;

        //npc
        foreach (NPC n in npc)
        {
            if (n.is_active)
            {
                n.gameObject.SetActive(true);
                n.StartCoroutine("ShowMent");
                GridManager.instance.CalculateObj(n.transform.position, ObjType.NPC);
            }
        }

        //출구
        //GridManager.instance.CalculateFlr(Define.door_pos[(int)Dir.down], FlrType.exit);
        //GridManager.instance.CalculateFlr(Define.door_pos[Define.door_count + (int)Dir.down], FlrType.exit);

        Fade.instance.FadeIn();

        yield return new WaitForSeconds(0.5f);

        controller.pause = false;
    }


    public IEnumerator StartGame()
    {
        controller.pause = true;
        //fade out
        Fade.instance.FadeOut(1.0f);
        Fade.instance.FadeIn_group(floor_cvs);  //층

        yield return new WaitForSeconds(1.0f);

        //caravan UI 비활성화
        caravan_ui.SetActive(false);
        
        //npc 비활성화        
        foreach (NPC n in npc)
        {
            if (n.is_active)
            {
                GridManager.instance.SendMessage("ResetObjNode", n.gameObject.transform.position);
            }
        }

        //출구 비활성화
       // GridManager.instance.SendMessage("ResetFlrNode", Define.door_pos[(int)Dir.down]);
       // GridManager.instance.SendMessage("ResetFlrNode", Define.door_pos[Define.door_count + (int)Dir.down]);

        //caravan 전체 비활
        caravan_map.SetActive(false);


        //게임 UI 활성화
        game_ui.SetActive(true);
        game_ui.transform.DOScale(1.0f, 0.5f);

        //게임 진행
        maps.Setting();
        game_door.SetActive(true);
        player_animate.Setting(true);
        player_animate.in_caravan = false;

        yield return new WaitForSeconds(1.0f);

        //fade in
        Fade.instance.FadeIn();
        Fade.instance.FadeOut_group(floor_cvs);

        this.enabled = false;

        yield return new WaitForSeconds(0.5f);
        controller.pause = false;
    }
}
