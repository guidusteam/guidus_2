﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Show_State : MonoBehaviour {

    public Text attack;
    public Text move_v;
    public Text shoot_v;
    public Text floor;
    public Slider hp_bar;
    public Slider st_bar;
    Player_State player_state;
	
	void Start () {
        player_state = GameObject.FindGameObjectWithTag("Player_Character").GetComponent<Player_State>();

        UpdateAbility();
        UpdateFloor();
	}
	
	void UpdateFloor()
    {
        floor.text = "-지하 " + player_state.floor.ToString() + "층-";
    }

    IEnumerator ShowHeadText( TextMesh text )
    {
        text.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        text.gameObject.SetActive(false);
    }

	void UpdateAbility () {
        attack.text = "공격력 : " + (player_state.attack_power+player_state.changed.attack_power).ToString();
        move_v.text = "이동속도 : " + ((1 - player_state.move_speed-player_state.changed.move_speed) * 100).ToString();
        shoot_v.text = "사격속도 : " + ((1 - player_state.shoot_speed-player_state.changed.shoot_speed) * 100).ToString();
    }

    public void Update_HP()
    {
        hp_bar.value = player_state.health / (player_state.max_health+player_state.changed.max_health);
    }

    public void Update_ST()
    {
        st_bar.value = player_state.stamina / (player_state.max_stamina+player_state.changed.max_stamina);
    }
}
