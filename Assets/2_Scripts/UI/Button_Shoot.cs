﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Button_Shoot : MonoBehaviour {

    public Camera main_camera;
    public Player_Animate player_animate;
    public Player_Shoot player_shoot;
    public Animator player_action;
    public Collider[] button;
    public Scrollbar left, right;

    Collider left_coll, right_coll;
    Ray ray;
    RaycastHit hit;
    
    void Start()
    {
        left_coll = left.GetComponent<Collider>();
        right_coll = right.GetComponent<Collider>();
    }

	//void Update ()
    //{
    //    GetEvent();
	//}


    void GetEvent()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Ended)
            {
                right.enabled = true;
                left.enabled = true;
                player_action.SetBool("shoot_button", false);
                player_animate.is_active = true;
                player_shoot.active = false;
            }
            ray = main_camera.ScreenPointToRay(touch.position);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("UI")))
            {
                if (hit.collider.CompareTag("Button_Shoot"))
                {
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            
                            player_action.SetBool("run", false);
                            player_action.SetBool("shoot_button", true);
                            player_animate.is_active = false;
                            player_shoot.active = true;
                            if (hit.collider == left_coll)
                            {
                                right.enabled = false;
                                //shoot.ShootBullet(left.value);
                            }
                            else
                            {
                                left.enabled = false;
                                //shoot.ShootBullet(-right.value);
                            }
                            break;

                        case TouchPhase.Moved:
                            if (hit.collider == left_coll)
                            {
                                right.enabled = false;
                                //shoot.ShootBullet(left.value);
                            }
                            else
                            {
                                left.enabled = false;
                                //shoot.ShootBullet(-right.value);
                            }
                            break;

                        case TouchPhase.Stationary:
                            if (hit.collider == left_coll)
                            {
                                right.enabled = false;
                                //shoot.ShootBullet(left.value);
                            }
                            else
                            {
                                left.enabled = false;
                                //shoot.ShootBullet(-right.value);
                            }
                            break;

                        
                    }

                }

                

            }
        }
    }
    
}
