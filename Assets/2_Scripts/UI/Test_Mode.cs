﻿using UnityEngine;
using System.Collections;


public class Test_Mode : MonoBehaviour {

    public GameObject test_buttons;

    public Mini_Map mini_scrt;
    public Maps maps;
    public Boss_Room boss_room;   

    float deltaTime = 0.0f;

    public bool is_active = false;
    
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
            Application.Quit();

        if (Input.GetKeyUp(KeyCode.Menu) || Input.GetKeyDown(KeyCode.T))
        {
            Check();
        }

        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
    }

    public void Check()
    {
        if (!is_active)
        {
            is_active = true;
            Test_Start();
        }
        else {
            is_active = false;
            Quit();
        }
    }

    void Test_Start () {
        test_buttons.SetActive(true);

        mini_scrt.is_test_mode = true;
        mini_scrt.SetIconsColor(maps.room_num);
	}

    void OnGUI()
    {
        if (is_active)
        {
            int w = Screen.width, h = Screen.height;

            GUIStyle style = new GUIStyle();

            Rect rect = new Rect(0, 700, w, h * 2 / 100);
            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = h * 2 / 100;
            style.normal.textColor = new Color(1.0f, 1.0f, 0.0f, 1.0f);
            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            GUI.Label(rect, text, style);
        }
    }
    
    public void KillEnemy()
    {
        if (maps.room_num == 9)  //보스방
            boss_room.KillBoss(false);
        else
            maps.set_obj[maps.room_num].KillEnemy(false);
    }

    void Quit()
    {
        test_buttons.SetActive(false);
        mini_scrt.is_test_mode = false;
        mini_scrt.SetIconsColor(maps.room_num);
    }
}
