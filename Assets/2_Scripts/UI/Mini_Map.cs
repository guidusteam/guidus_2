﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Mini_Map : MonoBehaviour {

    enum Dir { left, right, up, down };
    Image[] icons;
    public Image treasure;
    public Image boss;
    public Vector3[] icons_pos; //z값은 지나왔는지 여부 판단하는 값으로 사용
    public Maps maps;
    int num = 0;
    int len = 25;
    int treasure_idx, boss_idx;
    public bool is_test_mode = false;

    void Awake()
    {
        icons = new Image[Define.ROOM_NUM];
        for(int i = 0; i < icons.Length; ++i)
        {
            icons[i] = GameObject.Find("icon" + i).GetComponent<Image>();
            icons[i].gameObject.SetActive(false);
        }
    }

    public void Reset()
    {
        for (int i = 0; i < icons.Length; ++i)
        {
            icons[i].gameObject.SetActive(false);
        }
        treasure.gameObject.SetActive(false);
        boss.gameObject.SetActive(false);
        num = 0;
    }

    public void CreateIcons(int x, int y)
    {
        icons[num].transform.localPosition = new Vector3(400 + x * len, 250 + y * len, 0);
        icons_pos[num] = new Vector3(x, y, 0);  //z값 0으로 주어 리셋

        num++;
    }

    public void MoveIcons(int num)
    {
        Vector3 move_vec = new Vector3(0, 0, 0);

        switch ((Dir)num)
        {
            case Dir.left:
                move_vec = Vector3.right;
                break;

            case Dir.right:
                move_vec = Vector3.left;
                break;

            case Dir.up:
                move_vec = Vector3.down;
                break;

            case Dir.down:
                move_vec = Vector3.up;
                break;
        }

        for (int i = 0; i < icons.Length; ++i)
        {
            icons[i].transform.localPosition += move_vec * len;
            icons_pos[i] += move_vec;
        }
        treasure.transform.localPosition = icons[treasure_idx].transform.localPosition;
        boss.transform.localPosition = icons[boss_idx].transform.localPosition;
    }

    public void SetIconsColor(int room_idx)
    {
        //현재 위치한 방 흰색
        icons[room_idx].gameObject.SetActive(true);
        icons_pos[room_idx].z = 1;  //열어본 방
        icons[room_idx].color = new Color(1, 1, 1);

        for (int i = 0; i < icons.Length; ++i)
        {
            // 0,0에 위치한 방 흰색, 접한 방 밝은 회색으로 색 변경
            // 지나온 방 어두운 회색
            if (icons_pos[i].x == -1 && icons_pos[i].y == 0 && maps.door_scrt[room_idx].isDoor[0])  //왼쪽
            {
                icons[i].gameObject.SetActive(true);
                icons[i].color = new Color(0.5f, 0.5f, 0.5f);
                icons_pos[i].z = 1;  //파악된 방;
            }
            else if (icons_pos[i].x == 1 && icons_pos[i].y == 0 && maps.door_scrt[room_idx].isDoor[1])  //오른쪽
            {
                icons[i].gameObject.SetActive(true);
                icons[i].color = new Color(0.5f, 0.5f, 0.5f);
                icons_pos[i].z = 1;  //파악된 방
            }
            else if (icons_pos[i].x == 0 && icons_pos[i].y == 1 && maps.door_scrt[room_idx].isDoor[2])  //위쪽
            {
                icons[i].gameObject.SetActive(true);
                icons[i].color = new Color(0.5f, 0.5f, 0.5f);
                icons_pos[i].z = 1;   //파악된 방
            }
            else if (icons_pos[i].x == 0 && icons_pos[i].y == -1 && maps.door_scrt[room_idx].isDoor[3])  //아래쪽
            {
                icons[i].gameObject.SetActive(true);
                icons[i].color = new Color(0.5f, 0.5f, 0.5f);
                icons_pos[i].z = 1;   //파악된 방
            }
            else if(i != room_idx)
            {
                icons[i].color = new Color(0.2f, 0.2f, 0.2f);

                float x = Mathf.Abs(icons_pos[i].x);
                float y = Mathf.Abs(icons_pos[i].y);

                if(y > 1 || x > 2 || icons_pos[i].z == 0){
                   icons[i].gameObject.SetActive(false);
                }
                else if(y <= 1 && x <= 2    //3*5크기 안에 위치하고
                    && icons_pos[i].z == 1) //기존에 열어본 방이라면
                {
                    icons[i].gameObject.SetActive(true);
                }
                
            }
        }

        //공개된 보물방이고 테두리안에 있다면
        if (icons_pos[treasure_idx].z == 1 && icons[treasure_idx].IsActive())
            treasure.gameObject.SetActive(true);
        else
           treasure.gameObject.SetActive(false);

        //공개된 보스방이고 테두리안에 있다면
        if (icons_pos[boss_idx].z == 1 && icons[boss_idx].IsActive())
            boss.gameObject.SetActive(true);
        else 
            boss.gameObject.SetActive(false);


        //테스트모드일때는 모두 보이게
        if (is_test_mode) {
            for (int i = 0; i < icons.Length; ++i)
                icons[i].gameObject.SetActive(true);
            boss.gameObject.SetActive(true);
            treasure.gameObject.SetActive(true);
        }

    }

    public void SetSpecialIcon(int room_idx, int type)
    {
        if (type == 0)
        {  //보물방
            treasure_idx = room_idx;
            treasure.transform.localPosition = icons[treasure_idx].transform.localPosition;
        }

        else
        {    //보스방
            boss_idx = room_idx;
            boss.transform.localPosition = icons[boss_idx].transform.localPosition;
        }
    }
}
