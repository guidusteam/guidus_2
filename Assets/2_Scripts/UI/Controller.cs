﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Controller : MonoBehaviour {

    Player_Animate player_animate;
    Animator player_anim;
    Player_Shoot player_shoot;
    Vector3 att_dir, move_dir;
    bool is_shooting, is_moving;
    public bool pause = false;

    void Start () {
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();
        player_anim = player_animate.player_anim;
        player_shoot = GameObject.Find("ShootPoint").GetComponent<Player_Shoot>();
	}

    public void StartAttack()
    {
        if (pause)
        {
            QuitAttack();
            return;
        }
        is_shooting = true;
        if(!is_moving)
            player_anim.SetBool("shoot_button", true);

        att_dir = new Vector3(ETCInput.GetAxis("Right_X"), 0, ETCInput.GetAxis("Right_Y"));

        Quaternion look_rot = Quaternion.LookRotation(att_dir);
        Vector3 dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;

        player_animate.Rotate(dest_rot, true);
        player_shoot.ShootBullet(att_dir.normalized,is_moving);
    }

    public void QuitAttack()
    {
        player_anim.SetBool("shoot_button", false);
        player_anim.SetBool("backmove", false);
        player_anim.SetBool("move", false);
        is_shooting = false;
    }
   
    public void StartMove()
    {
        if (pause)
        {
            QuitMove();
            return;
        }
        is_moving = true;

        move_dir = new Vector3(ETCInput.GetAxis("Left_X"), 0, ETCInput.GetAxis("Left_Y"));
        
        Quaternion look_rot = Quaternion.LookRotation(move_dir);
        Vector3 dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;

        if (!is_shooting)
        {            
            player_anim.SetBool("run", true);
            player_animate.Rotate(dest_rot, false);
        }
        else    //걸으면서 쏘는 동작
        {
            if (Vector3.Angle(att_dir, move_dir) < 90)
            {
                player_anim.SetBool("backmove", false);
                player_anim.SetBool("move", true);
            }
            else
            {
                player_anim.SetBool("backmove", true);
                player_anim.SetBool("move", false);
            }
        }
        player_animate.Move(move_dir.normalized);
    }

    public void QuitMove()
    {
        player_anim.SetBool("run", false);
        player_animate.transform.DOKill();
        player_animate.camera_trans.DOKill();
        is_moving = false;
    }
}
