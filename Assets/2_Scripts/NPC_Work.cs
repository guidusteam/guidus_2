﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NPC_Work : MonoBehaviour {

    Player_Animate player_animate;
    GameObject npc_work;

   
    CanvasGroup npc_work_cvs;
    Text work_ment;

    void Start () {
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();
        npc_work = GameObject.Find("NPC_Work");
        npc_work_cvs = npc_work.GetComponent<CanvasGroup>();
        work_ment = GameObject.Find("work_ment").GetComponent<Text>();

    }

    public IEnumerator Work(NPCType type)
    {
        npc_work.SetActive(true);
        player_animate.is_active = false;

        Fade.instance.FadeIn_group(npc_work_cvs);

        work_ment.text = "Type : " + type.ToString();

        yield return new WaitForSeconds(0.5f);
               
    }

    public IEnumerator ExitWork()
    {
        Fade.instance.FadeOut_group(npc_work_cvs);
        player_animate.is_active = true;

        yield return new WaitForSeconds(0.5f);

        npc_work.SetActive(false);
    }
}
