﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Corpse : MonoBehaviour {

    public ParticleSystem corpse;
    public GameObject obj;
    GameObject barricades;
    Maps maps;
    Player_State player_state;
    Player_Animate player_animate;
    Player_Revive player_revive;
    BoxCollider collider;
    public int cor_room = -1;  //방 번호
    bool able_to_touch = false;

    void Start()
    {
        barricades = GameObject.Find("Barricade");
        maps = GameObject.Find("Maps").GetComponent<Maps>();
        player_state = GameObject.Find("Player").GetComponent<Player_State>();
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();
        player_revive = GameObject.Find("Player").GetComponent<Player_Revive>();

        collider = GetComponent<BoxCollider>();

        collider.enabled = false;
        obj.SetActive(false);
    }
    
    public void SetPosition()
    {
        cor_room = Random.Range(1, Define.ROOM_NUM - 2);
    }      

    public void Active()
    {
        if(maps.room_num == cor_room)
        {
            //맵에 보이게
            obj.SetActive(true);
            collider.enabled = true;

            corpse.Play();
        }
    }

    public void RemoveBarricades()
    {
        print("Remove");
        if (maps.room_num == cor_room)
        {
            barricades.transform.DOMoveY(-3, 0.5f);
            able_to_touch = true;
        }
    }

    void OnMouseDown()
    {
        //동작
        if(able_to_touch && Vector3.Distance(transform.position, player_state.transform.position) <= 3)
        {
            StartCoroutine("PickUp");
        }
    }

    IEnumerator PickUp()
    {
        player_animate.player_anim.CrossFade("pickup", 0.3f);

        yield return new WaitForSeconds(0.5f);

        player_state.SetValue(player_revive.result[0], player_revive.result[1],
        player_revive.result[2], player_revive.result[3], player_revive.result[4]);
        player_state.ChangedValueReset();

        player_state.Update_hp();
        player_state.Update_st();
        player_state.Update_ui();

        cor_room = -1;

        //제자리 후 없애기
        barricades.transform.DOMoveY(0, 0);
        collider.enabled = false;
        obj.SetActive(false);

        player_animate.Reset();
    }
}
