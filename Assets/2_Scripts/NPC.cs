﻿using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour {

    public bool is_active;
    public NPCType mytype;

    GameObject word_bubble;

    TextMesh word;
    NPC_Work npc_work;
    Player_Animate player_animate;

   

    void Start()
    {
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();

        word_bubble = GameObject.Find("word_bubble" + (int)mytype);
        word = word_bubble.GetComponentInChildren<TextMesh>();
        npc_work = GameObject.Find("Manager").GetComponent<NPC_Work>();

        word_bubble.SetActive(false);
    }

    void OnMouseDown()
    {
        if(Vector3.Distance(player_animate.transform.position,transform.position) <= 3)
            npc_work.StartCoroutine("Work", mytype);
    }

    public IEnumerator ShowMent()
    {
        for(int i = (int)mytype * Define.NPC_MENT_NUM; i < (int)(mytype + 1) * Define.NPC_MENT_NUM; ++i)
        {
            float time = Random.Range(2.0f, 7.0f);
            yield return new WaitForSeconds(time);

            word_bubble.SetActive(true);
            word.text = Define.NPC_MENT[i];

            time = Random.Range(3.0f, 6.0f);
            yield return new WaitForSeconds(time);

            word_bubble.SetActive(false);
        }
        StartCoroutine("ShowMent");
    }
}

