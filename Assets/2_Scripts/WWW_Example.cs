﻿using UnityEngine;
using System.Collections;

public class WWW_Example : MonoBehaviour {

    Player_State player_state;

    void Start()
    {
        //StartCoroutine("Call", url);
        player_state = GameObject.Find("Player").GetComponent<Player_State>();
    }

    IEnumerator Call()
    {
        string url = "http://localhost/echo.php";

        WWWForm cForm = new WWWForm();
        cForm.AddField("id", SystemInfo.deviceUniqueIdentifier);
        cForm.AddField("generation", player_state.generation);
        cForm.AddField("floor", player_state.floor);
        cForm.AddField("gold", player_state.gold);

        cForm.AddField("hp", player_state.health.ToString());
        cForm.AddField("stamina", player_state.stamina.ToString());
        cForm.AddField("max_hp", player_state.max_health.ToString());
        cForm.AddField("max_st", player_state.max_stamina.ToString());
        cForm.AddField("move_spd", player_state.move_speed.ToString());
        cForm.AddField("shoot_spd", player_state.shoot_speed.ToString());
        cForm.AddField("att_power", player_state.attack_power.ToString());
        cForm.AddField("shoot_path", (int)player_state.shoot_path);

        WWW wwwUrl = new WWW(url, cForm);

        yield return wwwUrl;

        Debug.Log(wwwUrl.text);
    }
}
